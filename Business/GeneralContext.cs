﻿using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using WebTA.Core;
using WebTA.Data;

namespace WebTA.Business
{
    public class GeneralContext
    {
        public static List<IWebElement> DisplayedElements(List<IWebElement> allElements) =>
            allElements.Where(a => a.Displayed).ToList();

        public static double CostStringToDouble(string costString) =>
            double.Parse(
                costString.Trim('$').Replace('.', ',')
                );

        public static void InsertDataIntoField<T>(T data, params IWebElement[] fieldsForInsert)
        {
            foreach (var field in fieldsForInsert)
            {
                field.SendKeys(Keys.Control + "a");
                field.SendKeys(Keys.Delete);
                field.SendKeys(data?.ToString());
            }
        }

        public static string? GetElementAttributeValue(string attributeName, IWebElement element) =>
            element.GetAttribute(
                attributeName.ToLower())?
            .ToString();

        public static void HoverAction(IWebElement element)
        {
            new Actions(DriverHolder.Driver)
                .MoveToElement(element)
                .Perform();
        }

        public static void SelectOptionValue(string option, IWebElement element)
        {
            SelectElement optionsObject = new SelectElement(element);
            optionsObject.SelectByText(option);
        }

        public static void MultipleClick(int quantity, IWebElement element)
        {
            for (int i = 0; i < quantity; i++)
            {
                element.Click();
            }
        }

        public static string GetBackgroundColor(IWebElement element) =>
            element.GetCssValue("background-color");

        public static bool? IsLinkContainsTheWord(string word, IWebElement element) =>
            GetElementAttributeValue("href", element)?.ToLower().Contains(word.ToLower());


        public static bool DoesElementExist(IWebElement element)
        {
            try
            {
                if (element.Displayed)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch
            {
                return false;
            }
        }

        public static void GoToStartPage()
        {
            DriverHolder.Driver.Navigate().GoToUrl(URIs.baseURI);
        }

        public static void RefreshPage()
        {
            DriverHolder.Driver.Navigate().Refresh();
        }

        public static void GoToPreviousPage() =>
            DriverHolder.Driver.ExecuteScript("window.history.go(-1)");

    }
}
