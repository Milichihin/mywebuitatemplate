﻿namespace WebTA.Data
{
    public class WaitersData
    {
        public static readonly int timeForWaitImplicit = 2;
        public static readonly double timeForSleep = 2;
        public static readonly int timeForWaitElementVisibility = 2;
        public static readonly int timeForWaitPageLoading = 2;
        public static readonly int timeForWaitElementClickable = 2;
    }
}
