﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace WebTA.Core
{
    public class DriverHolder
    {
        private static ChromeDriver? _instance;

        public static ChromeDriver Driver
        {
            get
            {
                return _instance ?? (_instance = new ChromeDriver());
            }
        }

        public static IWebElement DefineElement(string xPath) => Driver.FindElement(By.XPath(xPath));

        public static List<IWebElement> DefineElements(string xPath) => Driver.FindElements(By.XPath(xPath)).ToList();
    }
}

