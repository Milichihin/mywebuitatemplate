﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumExtras.WaitHelpers;
using System;
using System.Collections.Generic;
namespace WebTA.Core
{
    public class WaiterHelper
    {
        public static void SleepWaiter(double seconds)
        {
            Thread.Sleep(TimeSpan.FromSeconds(seconds));
        }

        public static void WaitForPageLoadComplete(long timeToWait)
        {
            new WebDriverWait(DriverHolder.Driver, TimeSpan.FromSeconds(timeToWait)).Until(WebDriver =>
            ((IJavaScriptExecutor)WebDriver)
                            .ExecuteScript("return document.readyState")
                            .Equals("complete"));
        }

        public static void ImplicitWait(long timeToWait)
        {
            DriverHolder.Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(timeToWait);
        }

        public static void WaitClickableElement(long timeToWait, IWebElement element)
        {
            WebDriverWait wait = new WebDriverWait(DriverHolder.Driver, new TimeSpan(timeToWait));
            wait.Until(ExpectedConditions.ElementToBeClickable(element));
        }
    }
}
