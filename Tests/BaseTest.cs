﻿using NUnit.Framework;
using WebTA.Business;
using WebTA.Core;
using WebTA.Data;

namespace WebTA.Tests
{
    public class BaseTest
    {
        [SetUp]
        public static void Start()
        {
            DriverHolder.Driver.Manage().Window.Maximize();
            GeneralContext.GoToStartPage();
            DriverHolder.Driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(WaitersData.timeForWaitImplicit);
        }

        [OneTimeTearDown]
        public void TearDown()
        {
            DriverHolder.Driver.Quit();
        }
    }
}
